# Pluralsight Crawler

## Setup

-   `yarn install`
-   Set `PluralsightAccessToken` environment variable with your Pluralsight JWT Token

## Usage

-   command: `node app (course-name) [skip]`
    -   `course-name`
        -   It is the course name in the url of the course
        -   It is the one with `-`
        -   It may not be called the same as the course's displayed name
    -   `skip`
        -   Skip 'n' number of videos and start download the 'nth' video in the course
