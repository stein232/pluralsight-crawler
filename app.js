const axios = require('axios')
const moment = require('moment')
const fs = require('fs')

let accessToken = process.env.PluralsightAccessToken

let headers = {
    cookie: `PsJwt-production=${accessToken}`,
}

// not confirmed list of quality
const quality = {
    high: '1280x720',
    medium: '853x480',
    low: '640x360',
}

// Set course name to download the course
// let courseName = 'aspdotnet-core-signalr-getting-started'
let courseId = process.argv[2]
let skip = process.argv[3] || 0

// file name format: `${i + 1}. ${module.title} - ${clip.index + 1}. ${clip.title}`

let course = {}
let directoryName = ''
let queue = []

getCourseData(courseId)
    .then(courseData => {
        course.title = courseData.title.replace(/\//g, '_').replace(/:/g, '_')
        course.author = courseData.authors[0].authorHandle
        course.displayDate = courseData.displayDate

        directoryName = `videos/${moment(course.displayDate).format(
            'YYYYMMDD'
        )} - ${course.title} - ${course.author}`

        if (!fs.existsSync(directoryName)) fs.mkdirSync(directoryName)

        for (let i = 0; i < courseData.modules.length; i++) {
            const module = courseData.modules[i]

            for (let j = 0; j < module.clips.length; j++) {
                const clip = module.clips[j]

                let queueItem = {
                    moduleIndex: i,
                    moduleTitle: module.title
                        .replace(/\//g, '_')
                        .replace(/:/g, '_'),
                    clipIndex: j,
                    clipId: clip.clipId,
                    clipTitle: clip.title
                        .replace(/\//g, '_')
                        .replace(/:/g, '_'),
                    duration: +clip.duration.substring(
                        2,
                        clip.duration.length - 1
                    ),
                }

                queue.push(queueItem)
            }
        }

        console.log('starting download')
        startDownloading()
    })
    .catch(e => {
        console.log(e)
    })

function startDownloading() {
    if (skip >= queue.length) {
        return
    }

    for (let i = 0; i < skip; i++) {
        queue.shift()
    }

    skip = 0

    let queueItem = queue.shift()
    if (queueItem !== undefined) {
        downloadClip(queueItem)
    }
}

function downloadClip(queueItem) {
    return getClipUrl(queueItem.clipId, quality.high).then(url => {
        console.log(
            `Downloading ${queueItem.moduleIndex + 1}. ${
                queueItem.moduleTitle
            } - ${queueItem.clipIndex + 1}. ${queueItem.clipTitle}`
        )

        let path = `${directoryName}/${queueItem.moduleIndex + 1}. ${
            queueItem.moduleTitle
        } - ${queueItem.clipIndex + 1}. ${queueItem.clipTitle}.mp4`

        const file = fs.createWriteStream(path)

        return axios({
            url,
            method: 'get',
            responseType: 'stream',
            headers,
        }).then(resp => {
            resp.data.pipe(file).on('finish', function() {
                console.log(
                    `Done ${queueItem.moduleIndex + 1}. ${
                        queueItem.moduleTitle
                    } - ${queueItem.clipIndex + 1}. ${queueItem.clipTitle}`
                )

                setTimeout(() => {
                    startDownloading()
                }, (queueItem.duration * 1000) / 2)
            })
        })
    })
}

// Gets course details via initial clipId
function getCourseData(courseId) {
    const url = `https://app.pluralsight.com/learner/content/courses/${courseId}`

    return axios.get(url, headers).then(({ data }) => {
        return data
    })
}

function getClipUrl(clipId, quality) {
    const url = 'https://app.pluralsight.com/video/clips/v3/viewclip'

    const payload = {
        clipId,
        mediaType: 'mp4',
        quality,
        online: true,
        boundedContext: 'course',
        versionId: '',
    }

    return axios.post(url, payload, { headers }).then(({ data }) => {
        return data.urls[0].url
    })
}
